# desafio-Inter

![Interinvestment](https://imgur.com/zP5cdon.png)

# Interinvestment 
[![NPM](https://img.shields.io/npm/l/react)](https://github.com/Demarqui/desafio-Inter/blob/main/LICENSE) 

# Sobre o projeto

O projeto Interinvestment é um projeto construído a partir de uma oportunidade cedida pelo banco Inter. 

Voltado para uma vaga de Dev lll no banco, o desafio consiste em uma aplicação back-end que realizará o(s) melhor(es) investimento(s) para o cliente, levando em consideração o valor passado pelo mesmo à investir, juntamente com a quantidade de empresas distintas desejada, visando sempre a diversificação.

A API fará uma lista de possíveis investimentos para o cliente, e escolherá aquele que retornar o menor troco para o solicitante.

## Modelo conceitual
![Modelo Conceitual](https://imgur.com/sFFOABw.png)

## Exemplo dos endpoints disponibilizados a partir do Swagger
![Modelo Conceitual](https://imgur.com/xA0PNPt.png)

# Tecnologias utilizadas
## Back end
- Java
- Spring Boot
- JPA / Hibernate
- Maven
- Banco de dados H2

# Como executar o projeto

## Back end
Pré-requisitos: Java 8 +

```bash
# clonar repositório
git clone https://github.com/Demarqui/desafio-Inter.git ou baixe o projeto em formato .zip através do botão de download disponibilizado pela plataforma.

# salvar o projeto em sua workspace e abri-lo com sua IDE
com o projeto dentro de sua workspace, o abra com a sua IDE e importe no caminho salvo.

# executar o projeto 
com o botão direito na classe DesafioInterApplication do pacote br.com.desafiointer, clique na opção Run Ass > Spring Boot App

# executar as operações do projeto
é possível efetuar as requisições dos endpoints do projeto através do swagger, que, através do próprio Spring framework, documentou todos os endpoints, onde estes estão disponíveis no link (http://localhost:8080/swagger-ui/index.html), que funcionará assim que o projeto for executado. Exemplo dos endpoints demonstrado acima.

# Autor

Douglas Louvison Demarqui

https://www.linkedin.com/in/douglasdemarqui/
